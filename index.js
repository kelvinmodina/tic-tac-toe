const express = require('express');
const app = express();
const http = require('http');
const server = http.createServer(app);
const { Server } = require("socket.io");
const io = new Server(server);

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/pages/index.html');
});

var players = 0;

io.on('connection', (socket) => {
    
    console.log(`player:${players} connected`);

    if(players>1){ //only 2 players are allowed
        socket.emit("invalid","invalid");
    }
   
    socket.on("userInput", (msg) => {

        console.log("on userInput",msg)
        io.emit("userInput", msg);
    })

    socket.on('disconnect', (socket) => {
        players--;
        console.log('a user disconnected');
    });
    players++;
});

io.on('disconnection', (socket) => {
    console.log('a user disconnected');
});

server.listen(3000, () => {
    console.log('listening on *:3000');
});